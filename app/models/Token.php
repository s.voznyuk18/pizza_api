<?php

namespace Models;

use \Illuminate\Database\Eloquent\Model;

class Token extends Model
{
  protected $table = 'tokens';

  protected $fillable = array('token', 'user_id');
}