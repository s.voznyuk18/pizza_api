<?php

namespace System;

use Models\Token;

class TokenManager
{
  private static function validateToken(string $token): bool
  {
    $createdAt = Token::where('token', '=', $token)->value('created_at');

    if (!$createdAt) {
      throw new \Exception('token invalid');
    }

    $createdAtDateTime = new \DateTime($createdAt);
    $createdAtInSeconds = $createdAtDateTime->getTimestamp();

    if (time() > $createdAtInSeconds + TOKEN_VALIDITY_DURATION) {
      self::removeToken($token);
      throw new \Exception('token has expired');
    }

    return true;
  }

  public static function createToken(string $userId): ?string
  {
    try {
      $token = bin2hex(random_bytes(16));

      $tokenInsertResult = Token::create(['user_id' => $userId, 'token' => $token]);

      $createdAt = $tokenInsertResult->toArray()['created_at'];
      $createdAtDateTime = new \DateTime($createdAt);
      $createdAtInSeconds = $createdAtDateTime->getTimestamp();

      setcookie('token', $token, $createdAtInSeconds + TOKEN_VALIDITY_DURATION, '/');
      return $token;

    } catch (\Illuminate\Database\QueryException $e) {
//      throw new \Exception('Failed to create token');
      return null;
    }
  }

  public static function getUserIdByToken(string $token): string
  {
    self::validateToken($token);
    $userID = Token::where('token', '=', $token)->value('user_id');
    return $userID;
  }

  public static function removeToken(string $token): bool
  {
    try {
      $token = Token::where('token', '=', $token)->first();

      if (!$token) {
        throw new \Exception('token not found');
      }

      setcookie('token', '', time() - TOKEN_VALIDITY_DURATION, '/');
      $token->delete();
      return true;
    } catch (\Illuminate\Database\QueryException $e) {
      $errorCode = $e->errorInfo[1];
      var_dump($errorCode);
      return false;
    } catch (\Exception $e) {
      var_dump($e->getMessage());
      return false;
    }
  }
}