<?php

namespace System;

class Router
{
  private array $routes = [];

  public function addRoute(string $path, array $rules): void
  {
    $this->routes[$path] = $rules;
  }

  public function processRoute(string $url, string $method): void
  {
    $routes = $this->routes;
    $method = strtolower($method);

    if (empty($routes)) {
      throw new \Exception('Routes not defined');
    }

    foreach ($routes as $routeUrl => $routeMethods) {
      if ($routeUrl === $url) {
        $controllerAction = $routeMethods[$method];
        break;
      }
    }

    if (!isset($controllerAction)) {
      header('Not fount', true, 404);
      exit();
    }

    [$controller, $action] = explode('@', $controllerAction);
    if (!isset($controller) && !isset($action)) {
      throw new \Exception('Invalid route');
    }

    $namespaceControllers = "Controllers\\$controller";

    $controllerObject = new $namespaceControllers();
    $controllerObject->$action();
  }
}

