<?php

namespace System;

class Response
{
  public static function send(int $status, string $message, object | array $data): void
  {
    http_response_code($status);

    header('Content-Type: application/json');

    $response = [
      'status' => $status,
      'message' => $message,
      'data' => $data
    ];

    $json_response = json_encode($response);

    echo $json_response;
  }
}