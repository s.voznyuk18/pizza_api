<?php

namespace Models;

use \Illuminate\Database\Eloquent\Model;
class User extends Model
{
  protected $table = 'Users';

  protected $fillable = array('first_name', 'email', 'password');
}