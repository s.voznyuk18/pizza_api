<?php

namespace App;

use System\Request;
use System\Router;
use Models\Database;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/constants.php';
require_once __DIR__ . '/config.php';

$db = Database::getInstance();

//var_dump($db);

try {
  $url = Request::getUrl();
  $method = Request::method();

  $router = new Router();
  $router->addRoute('/login', [
    'post' => 'AuthController@login'
  ]);
  $router->addRoute('/register', [
    'post' => 'AuthController@register'
  ]);

  $router->addRoute('/logout', [
    'post' => 'AuthController@logout'
  ]);

  $router->processRoute($url, $method);
} catch (\Exception $exception) {
  var_dump($exception->getMessage());
}