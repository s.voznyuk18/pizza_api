<?php

namespace Controllers;

use Models\User;
use System\TokenManager;
use System\Response;

class AuthController
{
  public function login(): void
  {
    try {
      $data = json_decode(file_get_contents('php://input'), true);
      extract($data);
      $user = User::where('email', '=', $email)->first();

      if (is_null($user)) {
        throw new \Exception('User not found', 404);
      }

      $token = TokenManager::createToken($user->id);

      if (is_null($token)) {
        throw new \Exception('Internal Server Error', 500);
      }

      Response::send(200, 'You are successfully logged in', $user);

    } catch (\Illuminate\Database\QueryException $e) {
      $errorCode = $e->errorInfo[1];

      if ($errorCode) {
        Response::send(500, 'Internal Server Error', array('result' => 'failure'));
      }

    } catch (\Exception $e) {
      Response::send($e->getCode(), $e->getMessage(), array('result' => 'failure'));
    }
  }

  public function register(): void
  {
    try {
      $data = json_decode(file_get_contents('php://input'), true);
      $password = password_hash($data['password'], PASSWORD_BCRYPT);
      $data['password'] = $password;
      $user = User::create($data);

      Response::send(200, 'You are successfully register', $user->toJson(JSON_PRETTY_PRINT));

    } catch (\Illuminate\Database\QueryException $e) {
      $errorCode = $e->errorInfo[1];

      if ($errorCode === '1062') {
        Response::send(
          409,
          'An account with the specified email address already exists',
          array('result' => 'failure')
        );
      }
    }
  }

  public function logout(): void
  {
    $data = json_decode(file_get_contents('php://input'), true);
    extract($data);
    $result = TokenManager::removeToken($token);

    if ($result) {
      Response::send(200, 'You are successfully logout', array('result' => 'success'));
      exit();
    }

    Response::send(401, 'Wrong token', array('result' => 'failure'));
  }
}