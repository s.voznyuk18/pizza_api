<?php

namespace Models;

use Illuminate\Database\Capsule\Manager as Capsule;

class Database
{

  private static ?Capsule $instance = null;

  private function __construct()
  {
  }

  private function __clone()
  {
  }

  public function __wakeup()
  {
  }

  public static function getInstance(): Capsule
  {
    if (!isset(self::$instance)) {
      $capsule = new Capsule;

      $capsule->addConnection([
        'driver' => DB_DRiVER,
        'host' => DB_HOST,
        'database' => DB_NAME,
        'username' => DB_USER,
        'password' => DB_PASS,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
      ]);

      $capsule->bootEloquent();
      self::$instance = $capsule;
    }
    return self::$instance;
  }
}