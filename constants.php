<?php
const APP_DIR = __DIR__ . '/';
const CONTROLLERS_DIR = APP_DIR . 'app/controllers';
const TOKEN_VALIDITY_DURATION = 3600 * 24 * 7;